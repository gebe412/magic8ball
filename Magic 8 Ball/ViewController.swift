//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by George Bedar on 6/17/19.
//  Copyright © 2019 George Bedar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  let ballArray = ["ball1","ball2","ball3","ball4","ball5"]
  var randomBallNumber: Int = 0
  
  
  

  @IBOutlet weak var ImageView: UIImageView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    ImageView.image = UIImage.init(named: ballArray[0])
    //ImageView.contentMode = UIView.ContentMode.scaleToFill  //the default
  
  }

  @IBAction func askButtonPressed(_ sender: Any) {
    
    newBallImage()
    
  }
  
  override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?)
  {
    newBallImage()
  }
  
  func newBallImage()  {
    randomBallNumber = Int.random(in: 0...4)
    ImageView.image = UIImage.init(named: ballArray[randomBallNumber])
  }
  
}

